@extends('layouts.app')

@section('content')
    <div class="jumbotron text-center" style="margin-bottom:0">
        <h1>Veterinaria</h1>
        <p>Ver Inventario de Farmacos</p> 
    </div>
    <div class="container" style="margin-top:30px">
            <div class="row justify-content-center">
                <div class="col-md-12 text-center">
                    @if(count($farmacos) > 0)   
                    <table class="table table-striped">
                            <thead>
                              <tr>
                                <th>Nombre</th>
                                <th>Cantidad</th>
                                <th>Precio</th>
                                <th>Opciones</th>
                                <th>Opciones</th>
                              </tr>
                            </thead>
                            <tbody>
                                    @foreach($farmacos as $farmaco)
                                    <tr>
                                            <td>{{$farmaco->nombre}}</td>
                                            <td>{{$farmaco->cantidad}}</td>
                                            <td>{{$farmaco->precio}}</td>
                                            <td>
                                              <a class="btn btn-primary mt-2 mt-sm-0" href="/farmacos/ver/{{$farmaco->id}}">Ver</a>
                                              <a class="btn btn-warning mt-2 mt-sm-0" href="/farmacos/ver/{{$farmaco->id}}/edit">Editar</a>
                                              
                                            </td>
                                            <td>
                                                {!! Form::open(['action' => ['FarmacosController@destroy', $farmaco->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
                                                {{Form::hidden('_method','DELETE')}}
                                                {{Form::submit('Delete', ['class' => 'btn btn-danger mt-2 mt-sm-0'])}}
                                              {!! Form::close() !!}
                                            </td>
                                          </tr>
                                @endforeach
                              
                            </tbody>
                          </table> 
                        {{$farmacos->links()}}
                    @else
                        <p>error! no existen farmacos registrados</p>
                    @endif
                </div>
            </div>
            <div class="space"></div>
    </div>
@endsection