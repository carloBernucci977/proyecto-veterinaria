@extends('layouts.app')

@section('content')
    <div class="jumbotron text-center" style="margin-bottom:0">
        <h1>Veterinaria</h1>
        <p>Editar Farmaco {{$farmaco->nombre}}</p> 
    </div>
    <div class="container" style="margin-top:30px">
            <div class="row justify-content-center">
                <div class="col-md-6">
                    {!! Form::open(['action' => ['FarmacosController@update', $farmaco->id], 'method' => 'POST']) !!}
                    <div class="form-group">
                        {{Form::label('nombre','Nombre')}}
                            {{Form::text('nombre', $farmaco->nombre, ['class' => 'form-control mr-sm-2 mb-2 mb-sm-0', 'placeholder' => 'Nombre'])}}
                          </div>
                          <div class="form-group">
                                {{Form::label('cantidad','Cantidad')}}
                            {{Form::text('cantidad', $farmaco->cantidad, ['class' => 'form-control mr-sm-2 mb-2 mb-sm-0', 'placeholder' => 'Cantidad'])}}
                          </div>
                          <div class="form-group">
                            {{Form::label('precio','Precio')}}
                        {{Form::text('precio', $farmaco->precio, ['class' => 'form-control mr-sm-2 mb-2 mb-sm-0', 'placeholder' => 'Precio'])}}
                      </div>
                        <div class="form-group row justify-content-center">
                            {{Form::hidden('_method','PUT')}}
                                {{Form::submit('Submit',['class'=>'btn btn-primary mt-2 mt-sm-0'])}}
                            </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="space"></div>
    </div>
@endsection