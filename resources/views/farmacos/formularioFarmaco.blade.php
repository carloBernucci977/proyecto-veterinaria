@extends('layouts.app')

@section('content')
    <div class="jumbotron text-center" style="margin-bottom:0">
        <h1>Veterinaria</h1>
        <p>Ingresar Nuevo Farmaco</p> 
    </div>
    <div class="container" style="margin-top:30px">
            <div class="row justify-content-center">
                <div class="col-md-12 text-center">
                            <h2>Nuevo Farmaco</h2>
                            {!! Form::open(['action' => 'FarmacosController@store', 'method' => 'POST']) !!}
                                <div class="form-group row justify-content-center">
                                        <div class="col-md-4">
                                        {{Form::text('nombre', '', ['class' => 'form-control mr-sm-2 mb-2 mb-sm-0', 'placeholder' => 'Nombre'])}}
                                        </div>
                                        <div class="col-md-4">
                                                {{Form::text('cantidad', '', ['class' => 'form-control mr-sm-2 mb-2 mb-sm-0', 'placeholder' => 'Cantidad'])}}
                                        </div>
                                        <div class="col-md-4">
                                            {{Form::text('precio', '', ['class' => 'form-control mr-sm-2 mb-2 mb-sm-0', 'placeholder' => 'Precio'])}}
                                    </div>
                                    </div>                                   
                                    <div class="form-group row justify-content-center">
                                        {{Form::submit('Submit',['class'=>'btn btn-primary mt-2 mt-sm-0'])}}
                                    </div>
                            {!! Form::close() !!}
                            
                </div>
            </div>
            <div class="space"></div>
            @include('inc.messages')

    </div>
@endsection