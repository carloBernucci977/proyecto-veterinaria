@extends('layouts.app')

@section('content')
    <div class="jumbotron text-center" style="margin-bottom:0">
        <h1>Veterinaria</h1>
        <p>Ver Clientes Registrados</p> 
    </div>
    <div class="container" style="margin-top:30px">
            <div class="row justify-content-center">
                <div class="col-md-12 text-center">
                    <h1>{{$cliente->nombre}}</h1>
                </div>
            </div>
            <div class="space"></div>
            <div class="row justify-content-center">
                <div class="col-md-6"><h3>Mascotas</h3></div>
                <div class="col-md-6 text-right"> <a class="btn btn-primary mt-2 mt-sm-0" href="{{ url('/mascotas/formulario/create') }}">Agregar Mascota</a></div>
            </div>
            <div class="row justify-content-center">
                    <div class="col-md-12 text-center">
                            @if(count($cliente->relacionMascota()) > 0)
                            <table class="table table-striped">
                                    <thead>
                                      <tr>
                                        <th>Nombre</th>
                                        <th>Raza</th>
                                        <th>Categoria</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                            @foreach($cliente->relacionMascota as $mascota)
                                            <tr>
                                                    <td>{{$mascota->nombre}}</td>
                                                    <td>{{$mascota->raza}}</td>
                                                    @foreach($categorias as $cat)
                                                        @if($cat->id == $mascota->categoria_id)
                                                        <td>{{$cat->nombre}}</td>    
                                                        @endif
                                                    @endforeach                                                    
                                            @endforeach
                                    </tbody>
                            </table>
                            @endif
                    </div>
                </div>
            <div class="space"></div>
    </div>
@endsection