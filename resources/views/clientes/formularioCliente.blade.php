@extends('layouts.app')

@section('content')
    <div class="jumbotron text-center" style="margin-bottom:0">
        <h1>Veterinaria</h1>
        <p>Ingresar Nuevo cliente</p> 
    </div>
    <div class="container" style="margin-top:30px">
            <div class="row justify-content-center">
                <div class="col-md-12 text-center">
                            <h2>Nuevo Cliente</h2>
                            {!! Form::open(['action' => 'ClientesController@store', 'method' => 'POST']) !!}
                                <div class="form-group row justify-content-center">
                                        <div class="col-md-4">
                                        {{Form::text('nombre', '', ['class' => 'form-control mr-sm-2 mb-2 mb-sm-0', 'placeholder' => 'Nombre'])}}
                                        </div>
                                        <div class="col-md-4">
                                                {{Form::text('direccion', '', ['class' => 'form-control mr-sm-2 mb-2 mb-sm-0' ,'placeholder' => 'Direccion'])}}
                                        </div>
                                        <div class="col-md-4">
                                                {{Form::text('telefono', '', ['class' => 'form-control mr-sm-2 mb-2 mb-sm-0', 'placeholder' => 'Telefono'])}}
                                        </div>
                                    </div>
                                    <div class="form-group row justify-content-center">
                                            <div class="col-md-4">
                                                {{Form::text('nombre_mascota', '', ['class' => 'form-control mr-sm-2 mb-2 mb-sm-0', 'placeholder' => 'Nombre de mascota'])}}
                                            </div>
                                            <div class="col-md-4">
                                                {{Form::text('raza', '', ['class' => 'form-control mr-sm-2 mb-2 mb-sm-0', 'placeholder' => 'Raza'])}}
                                            </div>
                                            <div class="col-md-4">{{ Form::select("categoria", $categorias, null, ["class"=>"form-control mr-sm-2 mb-2 mb-sm-0", "placeholder"=>"Selecccionar Categoria ...","required"]) }}</div>
                                    </div>
                                    <div class="form-group row justify-content-center">
                                        {{Form::submit('Submit',['class'=>'btn btn-primary mt-2 mt-sm-0'])}}
                                    </div>
                            {!! Form::close() !!}
                            
                </div>
            </div>
            <div class="space"></div>
            @include('inc.messages')

    </div>
@endsection