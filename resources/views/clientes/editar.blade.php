@extends('layouts.app')

@section('content')
    <div class="jumbotron text-center" style="margin-bottom:0">
        <h1>Veterinaria</h1>
        <p>Ver Clientes Registrados</p> 
    </div>
    <div class="container" style="margin-top:30px">
            <div class="row justify-content-center">
                <div class="col-md-6">
                    {!! Form::open(['action' => ['ClientesController@update', $cliente->id], 'method' => 'POST']) !!}
                    <div class="form-group">
                        {{Form::label('nombre','Nombre')}}
                            {{Form::text('nombre', $cliente->nombre, ['class' => 'form-control mr-sm-2 mb-2 mb-sm-0', 'placeholder' => 'Nombre'])}}
                          </div>
                          <div class="form-group">
                                {{Form::label('direccion','Direccion')}}
                            {{Form::text('direccion', $cliente->direccion, ['class' => 'form-control mr-sm-2 mb-2 mb-sm-0', 'placeholder' => 'Direccion'])}}
                          </div>
                        <div class="form-group">
                                {{Form::label('telefono','Telefono')}}
                            {{Form::text('telefono', $cliente->telefono, ['class' => 'form-control mr-sm-2 mb-2 mb-sm-0', 'placeholder' => 'Telefono'])}}
                        </div>
                        <div class="form-group row justify-content-center">
                            {{Form::hidden('_method','PUT')}}
                                {{Form::submit('Submit',['class'=>'btn btn-primary mt-2 mt-sm-0'])}}
                            </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="space"></div>
    </div>
@endsection