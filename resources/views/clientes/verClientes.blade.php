@extends('layouts.app')

@section('content')
    <div class="jumbotron text-center" style="margin-bottom:0">
        <h1>Veterinaria</h1>
        <p>Ver Clientes Registrados</p> 
    </div>
    <div class="container" style="margin-top:30px">
            <div class="row justify-content-center">
                <div class="col-md-12 text-center">
                    @if(count($clientes) > 0)   
                    <table class="table table-striped">
                            <thead>
                              <tr>
                                <th>Nombre</th>
                                <th>Direccion</th>
                                <th>Telefono</th>
                                <th>Opciones</th>
                                <th>Opciones</th>
                              </tr>
                            </thead>
                            <tbody>
                                    @foreach($clientes as $cliente)
                                    <tr>
                                            <td>{{$cliente->nombre}}</td>
                                            <td>{{$cliente->direccion}}</td>
                                            <td>{{$cliente->telefono}}</td>
                                            <td>
                                              <a class="btn btn-primary mt-2 mt-sm-0" href="/clientes/ver/{{$cliente->id}}">Ver</a>
                                              <a class="btn btn-warning mt-2 mt-sm-0" href="/clientes/ver/{{$cliente->id}}/edit">Editar</a>
                                              
                                            </td>
                                            <td>
                                                {!! Form::open(['action' => ['ClientesController@destroy', $cliente->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
                                                {{Form::hidden('_method','DELETE')}}
                                                {{Form::submit('Delete', ['class' => 'btn btn-danger mt-2 mt-sm-0'])}}
                                              {!! Form::close() !!}
                                            </td>
                                          </tr>
                                @endforeach
                              
                            </tbody>
                          </table> 
                        {{$clientes->links()}}
                    @else
                        <p>error! no existen clientes</p>
                    @endif
                </div>
            </div>
            <div class="space"></div>
    </div>
@endsection