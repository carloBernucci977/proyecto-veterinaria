@extends('layouts.app')

@section('content')
    <div class="jumbotron text-center" style="margin-bottom:0">
        <h1>Veterinaria</h1>
        <p>Clientes y sus mascotas</p> 
    </div>
    <div class="container" style="margin-top:30px">
            <div class="row justify-content-center">
                <div class="col-md-4 text-center">
                    <a class="btn btn-outline-primary btn-block" href="{{ url('/clientes/formulario/create') }}">Nuevo Cliente</a>
                </div>
                <div class="col-md-4 text-center">
                    <a class="btn btn-outline-primary btn-block" href="{{ url('/clientes/ver') }}">Ver Clientes</a>
                </div>
            </div>
            <div class="space"></div>
    </div>
@endsection