@extends('layouts.app')

@section('content')
    <div class="jumbotron text-center" style="margin-bottom:0">
        <h1>Veterinaria</h1>
        <p>Farmacos e inventario</p> 
    </div>
    <div class="container" style="margin-top:30px">
        <div class="row justify-content-center">
            <div class="col-md-4 text-center">
                <a class="btn btn-outline-primary btn-block" href="{{ url('/farmacos/formulario/create') }}">Agregar Farmaco</a>
            </div>
            <div class="col-md-4 text-center">
                <a class="btn btn-outline-primary btn-block" href="{{ url('/farmacos/ver') }}">Ver Farmacos</a>
            </div>
        </div>
        <div class="space"></div>
</div>
@endsection