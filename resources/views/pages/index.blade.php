@extends('layouts.app')

@section('content')    
    <div class="jumbotron text-center" style="margin-bottom:0">
        <h1>Veterinaria</h1>
        <p>Menu de administración</p> 
    </div>
    <div class="container" style="margin-top:30px">
        <div class="row justify-content-center">
            <div class="col-md-4 text-center">
                <a class="btn btn-outline-primary btn-block" href="{{ url('/clientes') }}">Clientes</a>
            </div>
            <div class="col-md-4 text-center">
                    <a class="btn btn-outline-primary btn-block" href="{{ url('/farmacos') }}">Farmacos</a>
                </div>
        </div>
        <div class="space"></div>
        <div class="row justify-content-center">
            <div class="col-md-4 text-center">
                    <a class="btn btn-outline-primary btn-block" href="{{ url('/receta') }}">Recetas</a>
                </div>
            <div class="col-md-4 text-center">
                    <a class="btn btn-outline-primary btn-block" href="{{ url('/sumario') }}">Sumario</a>
                </div>
        </div>
    </div>
@endsection