@extends('layouts.app')

@section('content')
    <div class="jumbotron text-center" style="margin-bottom:0">
        <h1>Veterinaria</h1>
        <p>Sumario</p> 
    </div>
    <div class="container" style="margin-top:30px">
        <div class="row justify-content-center">
            <div class="col-md-4 text-center">
                <h3>Recetas Generadas</h3>    
                <h6>{{$nRecetas}}</h6>    
            </div>
            <div class="col-md-4 text-center">
                <h3>Total de ventas</h3>
                <h6>$ {{$tRecetas}}</h6>
            </div>
        </div>
        <div class="space"></div>
        <div class="space"></div>
        <div class="space"></div>
        <div class="row justify-content-center">
            <div class="col-md-4 text-center">
                <h3>Cantidad de Clientes</h3>
                <h6>{{$nClientes}}</h6>
            </div>
            <div class="col-md-4 text-center">
        
            </div>
        </div>
    </div>
@endsection