@extends('layouts.app')

@section('content')
    <div class="jumbotron text-center" style="margin-bottom:0">
        <h1>Veterinaria</h1>
        <p>Ver Receta</p> 
    </div>
    <div class="container" style="margin-top:30px">
            <div class="row justify-content-center">
                <div class="col-md-12 text-center">
                    <h1>Receta N°{{$receta->id}}</h1>
                    <h6>Total: ${{$receta->total}}</h6>
                </div>
            </div>
            <div class="row justify-content-center">
                    <div class="col-md-12 text-center">
                            @if(count($receta->relacionFarmaco) > 0)   
                            <table class="table table-striped">
                                    <thead>
                                      <tr>
                                        <th>Farmaco</th>
                                        <th>Precio</th>
                                        <th>Cantidad</th>
                                      </tr>
                                    </thead>
                                    <tbody>
                                            @foreach($receta->relacionFarmaco as $farmaco)
                                            <tr>
                                                    <td>{{$farmaco->nombre}}</td>
                                                    <td>{{$farmaco->precio}}</td>
                                                    <td>{{$farmaco->pivot->cantidad}}</td>
                                            </tr>
                                            @endforeach
                                    </tbody>
                            </table>
                            @endif
                    </div>
                </div>
            <div class="space"></div>
    </div>
@endsection