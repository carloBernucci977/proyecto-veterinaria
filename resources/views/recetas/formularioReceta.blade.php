@extends('layouts.app')

@section('content')
    <div class="jumbotron text-center" style="margin-bottom:0">
        <h1>Veterinaria</h1>
        <p>Ingresar Nueva Receta</p> 
    </div>
    <div class="container" style="margin-top:30px">
            <div class="row justify-content-center">
                <div class="col-md-12 text-center">
                            <h2>Nueva Receta</h2>
                            {!! Form::open(['action' => 'RecetasController@store', 'method' => 'POST']) !!}
                                <div class="form-group row justify-content-center">
                                        <div class="col-md-8">
                                        {{Form::textarea('comentario', '', ['class' => 'form-control mr-sm-2 mb-2 mb-sm-0', 'placeholder' => 'Comentario'])}}
                                        </div>
                                        
                                    </div>
                                <div class="form-group row justify-content-center">
                                        <div class="col-md-4">
                                                {{Form::date('fecha', '', ['class' => 'form-control mr-sm-2 mb-2 mb-sm-0', 'placeholder' => 'Fecha'])}}
                                        </div>
                                        <div class="col-md-4">
                                            {{ Form::select('cliente', $clientes, null, array('class'=>'form-control mr-sm-2 mb-2 mb-sm-0', 'placeholder'=>'Selecccionar Cliente ...')) }}
                                        </div>
                                </div>
                                <div class="form-group row justify-content-center">
                                        <div class="col-md-8">
                                            <table class="table table-bordered" id="dynamic_field">
                                                <tr>
                                                    <td>Seleccionar Farmacos</td>
                                                    <td>Ingresar Cantidad</td>
                                                    <td><button type="button" name="add" id="add" class="btn btn-success">Agregar</button></td>
                                                </tr>
                                            </table>
                                        </div>
                                </div>                                   
                                <div class="form-group row justify-content-center">
                                    {{Form::submit('Submit',['class'=>'btn btn-primary mt-2 mt-sm-0'])}}
                                </div>
                            {!! Form::close() !!}
                </div>
            </div>
            <div class="space"></div>
            @include('inc.messages')
    </div>
    <script>{{ asset('js/app.js') }}</script>
    <script>
        $(document).ready(function(){
            var i=0;
            $('#add').click(function(){
                i++;
                $('#dynamic_field').append('<tr id="row'+i+'"><td>{{ Form::select("farmaco[][farmaco]", $farmacos, null, array("class"=>"form-control mr-sm-2 mb-2 mb-sm-0", "placeholder"=>"Selecccionar Farmaco ...","required")) }}</td><td>{{Form::text("cantidad[][cantidad]", null, ["class" => "form-control mr-sm-2 mb-2 mb-sm-0", "placeholder" => "Cantidad","required"])}}</td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></td></tr>');
            });
            
            $(document).on('click', '.btn_remove', function(){
                var button_id = $(this).attr("id"); 
                $('#row'+button_id+'').remove();
            });
            
        });
    </script>
@endsection