@extends('layouts.app')

@section('content')
    <div class="jumbotron text-center" style="margin-bottom:0">
        <h1>Veterinaria</h1>
        <p>Ver Recetas</p> 
    </div>
    <div class="container" style="margin-top:30px">
            <div class="row justify-content-center">
                <div class="col-md-12 text-center">
                    @if(count($recetas) > 0)   
                    <table class="table table-striped">
                            <thead>
                              <tr>
                                <th>ID</th>
                                <th>Cliente</th>
                                <th>Fecha</th>
                                <th>Total</th>
                                <th>Registro</th>
                                <th>Opciones</th>
                              </tr>
                            </thead>
                            <tbody>
                                    @foreach($recetas as $receta)
                                    <tr>
                                            <td>{{$receta->id}}</td>
                                            @foreach ($clientes as $cliente)
                                                @if ($cliente->id == $receta->cliente_id)
                                                <td>{{$cliente->nombre}}</td>        
                                                @endif
                                            @endforeach
                                            
                                            <td>{{$receta->fecha}}</td>
                                            <td>{{$receta->total}}</td>
                                            <td>
                                              <a class="btn btn-primary mt-2 mt-sm-0" href="/recetas/ver/{{$receta->id}}">Ver</a>                                              
                                            </td>
                                            <td>
                                                {!! Form::open(['action' => ['RecetasController@destroy', $receta->id], 'method' => 'POST', 'class' => 'pull-right'])!!}
                                                {{Form::hidden('_method','DELETE')}}
                                                {{Form::submit('Delete', ['class' => 'btn btn-danger mt-2 mt-sm-0'])}}
                                              {!! Form::close() !!}
                                            </td>
                                          </tr>
                                @endforeach
                              
                            </tbody>
                          </table> 
                        {{$recetas->links()}}
                    @else
                        <p>error! no existen recetas registradas</p>
                    @endif
                </div>
            </div>
            <div class="space"></div>
    </div>
@endsection