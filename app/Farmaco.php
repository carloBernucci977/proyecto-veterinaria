<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Farmaco extends Model
{
    public function relacionReceta(){
        return $this->belongsToMany(Receta::class, 'farmaco_receta','farmaco_id','receta_id');
    }

    
}
