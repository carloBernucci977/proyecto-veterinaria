<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mascota extends Model
{
    //
    public function relacionCliente(){
        return $this->belongsTo(Cliente::class);
    }

    public function relacionCategoria(){
        return $this->belongsTo(Categoria::class);
    }
}
