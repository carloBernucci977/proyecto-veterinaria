<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Receta extends Model
{
    public $timestamps = false;

    public function relacionFarmaco(){
        return $this->belongsToMany(Farmaco::class)->withPivot('cantidad','total');
    }

    //
    public function relacionCliente(){
        return $this->belongsTo(Cliente::class);
    }
}
