<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cliente;
use App\Mascota;
use App\Categoria;

class ClientesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clientes = Cliente::orderBy('nombre','desc')->paginate(5);
        return view('clientes.verClientes')->with('clientes',$clientes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorias = Categoria::all()->sortBy('nombre', SORT_NATURAL | SORT_FLAG_CASE)->pluck('nombre', 'id');;
        return view('clientes.formularioCliente')->with('categorias',$categorias);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'nombre' => 'required',
            'direccion' => 'required',
            'telefono' => 'required|numeric',
            'nombre_mascota' => 'required',
            'raza' => 'required'
        ]);
        $categoria = $request->input('categoria');
        $cliente = new Cliente;
        $cliente->nombre = $request->input('nombre');
        $cliente->direccion = $request->input('direccion');
        $cliente->telefono = $request->input('telefono');
        $cliente->save();
        $mascota = new Mascota;
        $mascota->nombre = $request->input('nombre_mascota');
        $mascota->raza = $request->input('raza');
        $mascota->cliente_id = $cliente->id;
        $mascota->categoria_id = $categoria;
        $mascota->save();
        $clientes = Cliente::orderBy('nombre','desc')->paginate(5);
        return view('clientes.verClientes')->with('clientes',$clientes);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $cliente = Cliente::find($id);
        $categorias = Categoria::all();
        return view('clientes.perfil')->with('cliente',$cliente)->with('categorias',$categorias);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cliente = Cliente::find($id);
        
        return view('clientes.editar')->with('cliente',$cliente);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'nombre' => 'required',
            'direccion' => 'required',
            'telefono' => 'required'
        ]);

        $cliente = Cliente::find($id);
        $cliente->nombre = $request->input('nombre');
        $cliente->direccion = $request->input('direccion');
        $cliente->telefono = $request->input('telefono');
        $cliente->save();
        $clientes = Cliente::orderBy('nombre','desc')->paginate(5);
        return view('clientes.verClientes')->with('clientes',$clientes);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $cliente = Cliente::find($id);
        $cliente->delete();
        $clientes = Cliente::orderBy('nombre','desc')->paginate(5);
        return view('clientes.verClientes')->with('clientes',$clientes);
    }
}
