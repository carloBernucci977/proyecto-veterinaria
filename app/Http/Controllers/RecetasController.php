<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Receta;
use App\Cliente;
use App\Farmaco;

class RecetasController extends Controller
{
    //
    //
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $recetas = Receta::orderBy('id','desc')->paginate(5);
        $clientes = Cliente::all();
        return view('recetas.verRecetas')->with('recetas',$recetas)->with('clientes',$clientes);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $farmacos = Farmaco::all()->sortBy('nombre', SORT_NATURAL | SORT_FLAG_CASE)->pluck('nombre', 'id');
        $clientes = Cliente::all()->sortBy('nombre', SORT_NATURAL | SORT_FLAG_CASE)->pluck('nombre', 'id');
        return view('recetas.formularioReceta ')->with('clientes',$clientes)->with('farmacos',$farmacos);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'comentario' => 'required',
            'fecha' => 'required',
            'cliente' => 'required',
            'farmaco' => 'required',
            'cantidad' => 'required'
        ]);
        $farmacos = $request->input('farmaco');
        $cantidades = $request->input('cantidad');
        $receta = new Receta;
        $receta->comentario = $request->input('comentario');
        $receta->cliente_id = $request->input('cliente');
        $receta->fecha = $request->input('fecha');
        $total = 0;
        $receta->total = $total;
        $receta->save();
        foreach ($farmacos as $key => $value) {
            if($cantidades[$key]['cantidad']!=0){
                $tempFarmaco = farmaco::find($farmacos[$key]['farmaco']);
                $tempTotal = $tempFarmaco->precio * $cantidades[$key]['cantidad'];
                $total = $total + $tempTotal;
                $receta->relacionFarmaco()->attach($farmacos[$key]['farmaco'],['cantidad' => $cantidades[$key]['cantidad'], 'total' => $tempTotal]);
            }
            
        }
        $receta->total = $total;
        $receta->save();
        //falta total
        
        $recetas = Receta::orderBy('id','desc')->paginate(5);
        return view('recetas.verRecetas')->with('recetas',$recetas);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $receta = Receta::find($id);
        return view('recetas.perfil')->with('receta',$receta);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $receta = Receta::find($id);
        return view('recetas.editar')->with('receta',$receta);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'comentario' => 'required',
            'fecha' => 'required',
            'cliente' => 'required'
        ]);

        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $receta = Receta::find($id);
        $receta->delete();
        $recetas = Receta::orderBy('id','desc')->paginate(5);
        return view('recetas.verRecetas')->with('recetas',$recetas);
    }
}
