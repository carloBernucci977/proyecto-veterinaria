<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Farmaco;

class FarmacosController extends Controller
{
    //
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $farmacos = Farmaco::orderBy('nombre','desc')->paginate(5);
        return view('farmacos.verFarmacos')->with('farmacos',$farmacos);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('farmacos.formularioFarmaco');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate($request, [
            'nombre' => 'required',
            'cantidad' => 'required',
            'precio' => 'required'
        ]);
        $oldFarmaco = Farmaco::where('nombre',$request->input('nombre'))->first();

        if($oldFarmaco!=null){
            $farmaco = Farmaco::find($oldFarmaco->id);
            $farmaco->nombre = $request->input('nombre');
            $farmaco->cantidad = $request->input('cantidad') + $oldFarmaco->cantidad;
            $farmaco->precio = $request->input('precio');
            $farmaco->save();
            $farmacos = Farmaco::orderBy('nombre','desc')->paginate(5);
            return view('farmacos.verFarmacos')->with('farmacos',$farmacos);
        }else{
            $farmaco = new Farmaco;
            $farmaco->nombre = $request->input('nombre');
            $farmaco->cantidad = $request->input('cantidad');
            $farmaco->precio = $request->input('precio');
            $farmaco->save();
            $farmacos = Farmaco::orderBy('nombre','desc')->paginate(5);
            return view('farmacos.verFarmacos')->with('farmacos',$farmacos);
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $farmaco = Farmaco::find($id);
        return view('farmacos.perfil')->with('farmaco',$farmaco);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $farmaco = Farmaco::find($id);
        return view('farmacos.editar')->with('farmaco',$farmaco);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'nombre' => 'required',
            'cantidad' => 'required',
            'precio' => 'required'
        ]);

        $farmaco = Farmaco::find($id);
        $farmaco->nombre = $request->input('nombre');
        $farmaco->cantidad = $request->input('cantidad');
        $farmaco->precio = $request->input('precio');
        $farmaco->save();
        $farmacos = Farmaco::orderBy('nombre','desc')->paginate(5);
        return view('farmacos.verFarmacos')->with('farmacos',$farmacos);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $farmaco = Farmaco::find($id);
        $farmaco->delete();
        $farmacos = Farmaco::orderBy('nombre','desc')->paginate(5);
        return view('farmacos.verFarmacos')->with('farmacos',$farmacos);
    }
}
