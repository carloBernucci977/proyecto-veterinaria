<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Receta;
use App\Cliente;

class PagesController extends Controller
{
    public function index(){
        
        return view('pages.index');
    }

    public function clientes(){
        return view('pages.clientes');
    }

    public function farmacos(){
        return view('pages.farmacos');
    }

    public function receta(){
        return view('pages.receta');
    }

    public function sumario(){
        $recetas = Receta::all();
        $clientes = Cliente::all();
        $nRecetas = sizeof($recetas);
        $tRecetas = 0;
        foreach($recetas as $receta){
            $tRecetas = $tRecetas + $receta->total;
        }
        $nClientes = sizeof($clientes);
        return view('pages.sumario')->with('nRecetas',$nRecetas)->with('tRecetas',$tRecetas)->with('nClientes',$nClientes);
    }
}
