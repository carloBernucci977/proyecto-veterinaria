<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    public function relacionMascota()
    {
        return $this->hasMany(Mascota::class);
    }
}
