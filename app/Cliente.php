<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model
{
    //
    protected $table = 'clientes';

    public function relacionMascota(){
        return $this->hasMany(Mascota::class);
    }

    public function relacionReceta(){
        return $this->hasMany(Receta::class);
    }
}
