<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFarmacoRecetaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();
        Schema::create('farmaco_receta', function (Blueprint $table) {
            //
            $table->bigIncrements('id');
            $table->unsignedBigInteger('receta_id');
            $table->foreign('receta_id')->references('id')->on('recetas')->onDelete('cascade');
            $table->unsignedBigInteger('farmaco_id');
            $table->foreign('farmaco_id')->references('id')->on('farmacos')->onDelete('cascade');
            $table->bigInteger('cantidad');
            $table->bigInteger('total');
        });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('farmaco_receta');
        Schema::enableForeignKeyConstraints();
    }
}
