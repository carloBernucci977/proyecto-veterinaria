<?php

use Illuminate\Database\Seeder;

class RecetasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('recetas')->insert([
            'comentario' => 'Receta Seeder',
            'fecha' => '2019-06-26',
            'total' => 21500,
            'cliente_id' => '1'
        ]);

        DB::table('farmaco_receta')->insert([
            'receta_id' => '1',
            'farmaco_id' => '1',
            'cantidad' => 1,
            'total' => 15000
        ]);

        DB::table('farmaco_receta')->insert([
            'receta_id' => '1',
            'farmaco_id' => '2',
            'cantidad' => 1,
            'total' => 6500
        ]);
    }
}
