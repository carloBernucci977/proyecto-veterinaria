<?php

use Illuminate\Database\Seeder;

class FarmacosTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('farmacos')->insert([
            'nombre' => "Panadol",
            'cantidad' => 50,
            'precio' => 15000
        ]);
        DB::table('farmacos')->insert([
            'nombre' => "Anestesia",
            'cantidad' => 50,
            'precio' => 6500
        ]);
    }
}
