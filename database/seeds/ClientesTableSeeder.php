<?php

use Illuminate\Database\Seeder;

class ClientesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('clientes')->insert([
            'nombre' => 'Juan Pereira',
            'direccion' => 'Javiera Carrera 645',
            'telefono' => '845315648'
        ]);
        DB::table('clientes')->insert([
            'nombre' => 'Diego Ojeda',
            'direccion' => 'Javiera Carrera 55',
            'telefono' => '484454254'
        ]);
    }
}
