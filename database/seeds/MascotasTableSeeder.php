<?php

use Illuminate\Database\Seeder;

class MascotasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('mascotas')->insert([
            'nombre' => 'Juanito',
            'raza' => 'Ragdoll',
            'cliente_id' => '1',
            'categoria_id' => '1'
        ]);
        DB::table('mascotas')->insert([
            'nombre' => 'Challan',
            'raza' => 'Akita',
            'cliente_id' => '2',
            'categoria_id' => '2'
        ]);
        DB::table('mascotas')->insert([
            'nombre' => 'Wiles smit',
            'raza' => 'Bayo',
            'cliente_id' => '2',
            'categoria_id' => '3'
        ]);
    }
}
