<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(FarmacosTableSeeder::class);
        $this->call(CategoriasTableSeeder::class);
        $this->call(ClientesTableSeeder::class);
        $this->call(MascotasTableSeeder::class);
        $this->call(RecetasTableSeeder::class);
    }
}
