<?php

use Illuminate\Database\Seeder;

class CategoriasTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categorias')->insert([
            'nombre' => 'Gato'
        ]);
        DB::table('categorias')->insert([
            'nombre' => 'Perro'
        ]);
        DB::table('categorias')->insert([
            'nombre' => 'Caballo'
        ]);
        DB::table('categorias')->insert([
            'nombre' => 'Hamster'
        ]);
    }
}
