<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index');

Route::get('/clientes', 'PagesController@clientes');

Route::get('/receta', 'PagesController@receta');

Route::get('/farmacos', 'PagesController@farmacos');

Route::get('/sumario', 'PagesController@sumario');

Route::resource('/clientes/formulario', 'ClientesController');

Route::resource('/clientes/ver', 'ClientesController');

Route::resource('/farmacos/formulario', 'FarmacosController');

Route::resource('/farmacos/ver', 'FarmacosController');

Route::resource('/recetas/formulario', 'RecetasController');

Route::resource('/recetas/ver', 'RecetasController');